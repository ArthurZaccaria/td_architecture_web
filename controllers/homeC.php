<?php

class HomeController{
  public function __construct(){  }
  public function bydefault(){
    $this->viewpublic();
  }
  public function viewpublic(){
    $title='archiweb-tds / Home';
    $content=file_get_contents(VIEWSDIR.DS.'home_viewpublic.php');
    include_once VIEWSDIR.DS.'template.php';
  }
}
