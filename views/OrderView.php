<?php
class OrderView{
	public function __construct()
	{
		
	}
	public function listall($allorders)
	{
		$content= <<<EOT
		
		<h1 class="mt-5">Liste des commandes</h1>

		<div class="row">
		  <a href="index.php?controller=order&method=create"><button type="button" class="btn btn-primary">Créer une nouvelle commande</button></a>
		</div>

		<div class="row">
		  <table class="table table-stiped">
		  <thead>
			<tr>
			  <th>scope="col">#</th>
			  <th>scope="col">OrderDate</th>
			  <th>scope="col">CustomerID</th>
			  <th>scope="col">TotalDue</th>
			</tr
		  </table>
		  <tbody>
	EOT;
	
		foreach ($allorders as $oneorder ){
			$content.='<tr>';
			$content.='<td>'.$oneorder->SalesOrderID.'</td>';
			$content.='<td>'.$oneorder->OrderDate.'</td>';
			$content.='<td>'.$oneorder->CustomerID.'</td>';
			$content.='<td>'.$oneorder->TotalDue.'</td>';
			$content.='</tr>';
			
		}
		$content.='...</tbody>';
		$content.='.</table>';
		$content.='...</div>';		
		$title='archiweb-tds / Toutes les commandes';
		include_once VIEWSDIR.DS.'template.php';
	}
}