<?php
define('DS', DIRECTORY_SEPARATOR);
define('ROOTDIR', dirname(__FILE__));
define('CONTROLLERSDIR', ROOTDIR.DS.'controllers');
define('VIEWSDIR', ROOTDIR.DS.'views');
define('MODELSDIR', ROOTDIR.DS.'model');

if (isset($_GET['url'])){
	$str = explode('/', $url);
	$taille = sizeof($str);
	if ($taille == 1){
		$controller = $taille[0];
	}else if ($taille == 2){
		$controller = $taille[0];
		$method = $taille[1];
	}else{
		$controller = $taille[0];
		$method = $taille[1];
		$id = $taille[2];
	}
	
}else{
	$controller='home';
	$method='bydefault';
	$id='null';
}


/*if (isset($_GET['controller'])){
  $controller=$_GET['controller'];
}else{
  $controller='home';
}
if (isset($_GET['method'])){
  $method=$_GET['method'];
}else{
  $method='bydefault';
}
if (isset($_GET['id'])){
  $id=$_GET['id'];
}else{
  $id='null';
}*/

require_once CONTROLLERSDIR.DS.$controller.'C.php';
$classname=ucfirst($controller).'Controller';

$c=new $classname();
if(is_null($id)) $c->$method();
else $c->$method($id);
